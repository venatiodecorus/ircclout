var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

const IrssiParser = require('./parser');
const irssiParser = new IrssiParser();

var date = new Date();

irssiParser.startParse();

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', indexRouter);
/* GET home page. */
app.use('/', function(req, res, next) {
    irssiParser.setTop();
    res.render('index', { title: 'IRC Clout', top: irssiParser.getTop(), startTime: date.toUTCString() });
    //res.render('index', { title: 'IRC Clout', top: [{nick:'octopus',count:30},{nick:'jewbird',count:50}] });
  });
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
