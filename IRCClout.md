IRCClout.com
============

This program will parse IRC logs and generate rankings based on mentions and the weights of the users who make the mention. It will output a leaderboard which will be displayed on ircclout.com. I don't want to have one of my servers constantly DDoS'd so I'm going to put it up on a static host like gitlab pages. Maybe I'll put it behind CloudFlare as well.

- Identify users to track
- Parse logs for mentions
- Some method to prevent spam/gaming of the system
- Process mentions based on criteria (clout of user who made the mention) and adjust user clout
- Generate the leaderboard page

I think I'll just use an instance of irssi to collect logs that will be provided to the program. I'll have that running in a container (same container as the app or no?) and that client will connect and monitor certain channels.

Small, fast, in memory storage for user information. 