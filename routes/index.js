var express = require('express');
const IrssiParser = require('../parser');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(IrssiParser.getTop());
  res.render('index', { title: 'IRC Clout' });
});

module.exports = router;
