const fs = require('fs');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');

class IrssiParser {
    constructor() {
        this.path = '/home/octo/irclogs2/efnet/#ircclout.log';
        this.file = null;
        this.readbytes = 0;
        this.byte_size = 256;
        this.regex = /\d+:\d+ <(.)(.+)> (.+)/m;
        this.top = [];

        db.run("CREATE TABLE counts (nick TEXT UNIQUE, count INTEGER)");
    }

    startParse() {
        var spawn = require('child_process').spawn;
        var tail = spawn('tail', ['-f', this.path]);

        tail.stdout.on('data', (data) => {
            var string = data.toString().trim();
            var lines = string.split('\n');
            lines.forEach((value, index) => {
                var line = this.regex.exec(value);
                if(line != null) {
                    this.processMessage(line);
                }
            })
        })

    }

    processMessage(line) {
        console.log("Processing line: " + line[0]);
        // Check nick
        db.serialize(() => {
            db.get("SELECT nick, count FROM counts WHERE nick = (?)", line[2], (err, row) => {
                if(row == null) {
                    try {
                    db.run("INSERT OR REPLACE INTO counts VALUES (?,?)", [line[2],0]);
                    } catch(err) {
                        console.log(err);
                    }
                }
            })
        })

        // Process message
        db.serialize(() => {
            // get rid of colons cuz they are often following nicks that are tab completed
            let strings = line[3].replace(/:$/,'').split(' ').filter(l => l != line[2]);
            db.each("SELECT nick, count FROM counts WHERE nick IN (?)", strings.join(','), (err, row) => {
                db.run("UPDATE counts set count = (?) WHERE nick = (?)", [row.count += 1, row.nick]);
                console.log("Adding count for user " + row.nick + " count: " + row.count);
            })
        })
    }

    // getTop returns the top 10 nicks from the database
    setTop() {
        db.all("SELECT nick, count FROM counts WHERE count > 0 ORDER BY count DESC LIMIT 10", (err, rows) => {
            if(rows != null) {
                this.top = rows;
            }
        })
        // db.serialize(() => {
        //     db.all("SELECT nick, count FROM counts WHERE count > 0 ORDER BY count DESC LIMIT 10", (err, rows) => {
        //         if(rows != null) 
        //             return rows;
        //     })
        // })
        setTimeout(() => this.setTop(), 3000);
    }

    getTop() {
        return this.top;
    }
}

module.exports = IrssiParser;